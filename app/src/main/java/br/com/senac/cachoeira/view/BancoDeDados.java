package br.com.senac.cachoeira.view;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BancoDeDados extends SQLiteOpenHelper {


    private static final String DATABASE = "SQLte" ;
    private static final int VERSAO = 1 ;



    public BancoDeDados(Context context) { super( context, DATABASE , null, VERSAO); }

    @Override
    public void onCreate(SQLiteDatabase SQLD) {
        String ddl = "CREATE TABLE Cachoeiras " +
                "( id INTEGER PRIMARY KEY , " +
                " nome TEXT NOT NULL ," +
                "telefone TEXT , " +
                "email TEXT , " +
                "site TEXT ) ; " ;
        SQLD.execSQL(ddl);
    }

    @Override
    public void onUpgrade(SQLiteDatabase SQLD, int i, int i1) {
        String ddl = "DROP TABLE IF EXISTS Cachoeiras ; " ;
        SQLD.execSQL(ddl);
        this.onCreate(SQLD);
    }

    public void Salvar(Cachoeira cachoeira) {

        ContentValues Values = new ContentValues();
        Values.put("nome" , cachoeira.getNome());
        Values.put("telefone" , cachoeira.getTelefone());
        Values.put("email" , cachoeira.getEmail());
        Values.put("site" , cachoeira.getSite());

        getWritableDatabase().insert(
                "cachoeiras" ,
                null ,
                Values);
        }

    public List<Cachoeira> getLista() {
        List<Cachoeira> lista = new ArrayList<>();
        String cacho[] = {"id","nome","telefone","email","site"} ;

        Cursor cursor =  getWritableDatabase().query(
                "CachoeiraS",
                cacho,
                null,
                null,
                null,
                null,
                null);

        while (cursor.moveToNext()) {
            Cachoeira cachoeira = new Cachoeira();
            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setTelefone(cursor.getString(2));
            cachoeira.setEmail(cursor.getString(3));
            cachoeira.setSite(cursor.getString(4));
            lista.add(cachoeira);
        }
        return lista;
}
}

