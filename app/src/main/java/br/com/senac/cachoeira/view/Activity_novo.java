package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.R;

public class Activity_novo extends AppCompatActivity {
    public static final  int  REQUEST_IMAGE_CAPTURE = 1 ;
    private static Bitmap imagem   ;



    public static Bitmap getImagem(){
        return imagem;
    }

    private ImageView imageView ;
    private EditText Nome ;
    private RatingBar Classificacao ;
    private EditText email;
    private EditText Site;
    private EditText Telefone;
    private Button buttonSalvar ;

    private BancoDeDados BancoCachoeira;



    private Cachoeira cachoeira ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView  = findViewById(R.id.foto);
        Nome = findViewById(R.id.TexNome);
        Classificacao = findViewById(R.id.Classificacao);
        buttonSalvar = findViewById(R.id.ButonSalvar);
        Telefone = findViewById(R.id.TexTelefone);
        email = findViewById(R.id.EmailTT);
        Site = findViewById(R.id.TexSite);

        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cachoeira = new Cachoeira();

                cachoeira.setNome(Nome.getText().toString());
                cachoeira.setTelefone(Telefone.getText().toString());
                cachoeira.setEmail(email.getText().toString());
                cachoeira.setSite(Site.getText().toString());
                /*try {*/
                    BancoCachoeira = new BancoDeDados(Activity_novo.this);
                    BancoCachoeira.Salvar(cachoeira);
                    BancoCachoeira.close();
                    Toast.makeText(Activity_novo.this , "Salvo comsucesso." , Toast.LENGTH_LONG).show();

                    finish();


                /*}catch (Exception ece){
                    Toast.makeText(Activity_novo.this , "Erro ao Salvar." , Toast.LENGTH_LONG).show();
                }*/


            }

        });

    }
     public void capturarImagem(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE) ;
        if(intent.resolveActivity(getPackageManager()) != null ){
            startActivityForResult(intent , REQUEST_IMAGE_CAPTURE);
        }
    }
    private boolean preenchido(String fui) { return (fui != null && !fui.isEmpty());}

    public void valiardados() throws Exception {
        List<String> listarequeridos = new ArrayList<>();
         if (!preenchido(cachoeira.getNome())) {
             listarequeridos.add("Nome");
             Nome.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
         }
         if (!preenchido(cachoeira.getEmail())) {
            listarequeridos.add("email");
        }
        if (!preenchido(cachoeira.getTelefone())) {
            listarequeridos.add("telefone");
        }
        if (!preenchido(cachoeira.getSite())) {
            listarequeridos.add("site");
        }
        if (listarequeridos.size() > 0) {
             throw new Exception("campos requridos " + listarequeridos.toString());
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras()  ;
            imagem =  (Bitmap) extras.get("data") ;
            imageView.setImageBitmap(imagem);
        }
    }
      public void salvar (View view){
        String nome = Nome.getText().toString() ;
        float classificacao = Classificacao.getRating() ;
        Bitmap bmp = imageView.getDrawingCache() ;

        cachoeira = new Cachoeira();
        cachoeira.setNome(nome);
        cachoeira.setClassificacao(classificacao);
        cachoeira.setImagem(bmp);

        Intent intent = new Intent();
        intent.putExtra(MainActivity.CACHOEIRA , cachoeira) ;
        setResult(RESULT_OK , intent);
        finish();
    }






























}
