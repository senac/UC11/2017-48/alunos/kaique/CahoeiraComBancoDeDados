package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.R;

public class MainActivity extends AppCompatActivity {
    public static final  int  REQUEST_NOVO = 1 ;
    private static Bitmap imagem   ;
    public static Bitmap getImagem(){
        return imagem;
    }
    public static String CACHOEIRA = "cachoeira" ;
    public static final String  IMAGEM =  "imagem" ;
    private ListView listView  ;
    private List<Cachoeira> lista  = new ArrayList<>() ;
    private ArrayAdapter<Cachoeira> adapter  ;
    private ListView listViewCachoeiras;
    private BancoDeDados BancoCachoeira;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);
        return true ;
    }
    public void novo(MenuItem item){
        Intent intent = new Intent(this , Activity_novo.class);
        startActivityForResult(intent , REQUEST_NOVO);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_NOVO){
            switch (resultCode){
            case RESULT_OK :
                 Cachoeira cachoeira = (Cachoeira) data.getSerializableExtra(MainActivity.CACHOEIRA);
                 cachoeira.setImagem(Activity_novo.getImagem());
                 lista.add(cachoeira);
                 
                 break;
            case RESULT_CANCELED :
                 Toast.makeText(this, "Cancelou" , Toast.LENGTH_LONG).show();
                 break;
}
}
}
    public void sobre (MenuItem item){
        Intent intent = new Intent(this , Activity_Sobre.class) ;
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();

        listViewCachoeiras = findViewById(R.id.ListaCachoeiras);
        BancoCachoeira = new BancoDeDados(this);
        lista = BancoCachoeira.getLista ();
        BancoCachoeira.close();

        adapter = new ArrayAdapter<Cachoeira>(this , android.R.layout.simple_list_item_1 , lista);

        listViewCachoeiras.setAdapter(adapter);
    }



















}
